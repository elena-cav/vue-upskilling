module.exports = {
  extends: ["plugin:vue/vue3-recommended"],
  rules: {
    "vetur.validation.style": "off",
    // override/add rules settings here, such as:
    // 'vue/no-unused-vars': 'error'
  },
};
